﻿using Common;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client
{
    public class Program
    {
        static void Main(string[] args)
        {
            MainAsync().Wait();
        }

        static async Task MainAsync()
        {
            string baseUri = "http://localhost:8888";
            HttpClient http = new HttpClient();
            Client client = new Client(http, baseUri);

            bool isContinue = true;
            Console.WriteLine("To exit press enter");
            while (isContinue)
            {
                Console.WriteLine($"Base uri: {baseUri}");
                ShowMenu();
                string selection = Console.ReadLine();
                if (string.IsNullOrEmpty(selection))
                {
                    return;
                }
                Enum.TryParse(selection, out UrlType urlType);

                if (urlType == UrlType.MyNameByCookies)
                {
                    var cookies = await client.SendCookiesRequest($"{baseUri}/{UrlType.MyNameByCookies}/");
                    foreach (Cookie cookie in cookies)
                    {
                        Console.WriteLine($"Cookie name: {cookie.Name}");
                        Console.WriteLine($"Cookie value: {cookie.Value}");
                    }
                }
                else
                {
                    var response = await client.SendRequest($"{baseUri}/{urlType}/", urlType);
                    Console.WriteLine($"Request: {baseUri}/{urlType}/");
                    Console.WriteLine("Response:");
                    await ShowResponse(response);
                }
            }
        }

        public static void ShowMenu()
        {
            Console.WriteLine("0 - MyName");
            Console.WriteLine("1 - Information");
            Console.WriteLine("2 - Success");
            Console.WriteLine("3 - Redirection");
            Console.WriteLine("4 - ClientError");
            Console.WriteLine("5 - ServerError");
            Console.WriteLine("6 - MyNameByHeader");
            Console.WriteLine("7 - MyNameByCookies");
        }

        public static async Task ShowResponse(HttpResponseMessage response)
        {
            Console.WriteLine($"    Status code: {(int)response.StatusCode}");
            Console.WriteLine($"    Content: {await response.Content.ReadAsStringAsync()}");
            Console.WriteLine($"    Headers: ");
            foreach (var header in response.Headers)
            {
                Console.WriteLine($"        {header.Key}:");

                foreach (var value in header.Value)
                {
                    Console.WriteLine($"            {value}");

                }
            }
        }
    }
}
