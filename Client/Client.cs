﻿using Common;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client
{
    public class Client
    {
        private readonly HttpClient _http;
        private readonly string _uri;
        private readonly CookieContainer _cookies = new CookieContainer();
        private readonly string _cookieHeader = "Set-Cookie";
        public Client(HttpClient http, string uri)
        {
            _http = http ?? throw new ArgumentNullException(nameof(http));
            _uri = uri ?? throw new ArgumentNullException(nameof(uri));
        }

        public async Task<HttpResponseMessage> SendRequest(string uri, UrlType urlType)
        {
            HttpResponseMessage response = await _http.GetAsync(uri);
            return response;
        }

        public async Task<CookieCollection> SendCookiesRequest(string uri)
        {
            if (_cookies.Count != 0)
            {
                Console.WriteLine("[INFO]: Cookies retrieved");
                return _cookies.GetCookies(new Uri(uri));
            }

            HttpResponseMessage response = await _http.GetAsync(uri);
            var cookies = response.Headers.GetValues(_cookieHeader);
            var cookiesString = cookies.Aggregate((c1, c2) => c1 + ", " + c2);
            _cookies.SetCookies(new Uri(uri), cookiesString);
            Console.WriteLine("[INFO]: Cookies saved");
            return _cookies.GetCookies(new Uri(uri));
        }
    }
}
