﻿namespace Common
{
    public enum UrlType
    {
        MyName = 0,
        Information,
        Success,
        Redirection,
        ClientError,
        ServerError,
        MyNameByHeader,
        MyNameByCookies
    }
}
