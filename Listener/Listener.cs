﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Net.Http;
using System.IO;
using System.Reflection;
using System.Linq;
using Common;
using System.Threading.Tasks;

namespace Listener
{
    public class Listener
    {
        private readonly string _uri;
        private HttpListener _http;
        private readonly string _name = "Mariia";
        public Listener(HttpListener http, string uri)
        {
            _http = http ?? throw new ArgumentNullException(nameof(http));
            _uri = uri ?? throw new ArgumentNullException(nameof(uri));
            InitPrefixes();
        }
        public async Task StartListening()
        {
            if (!HttpListener.IsSupported)
            {
                Console.WriteLine("Windows XP SP2 or Server 2003 is required to use the HttpListener class.");
                return;
            }

            _http.Start();
            Console.WriteLine("Listening...");

            while (true)
            {
                HttpListenerContext context = await _http.GetContextAsync();
                HttpListenerRequest request = context.Request;
                HttpListenerResponse response = context.Response;

                var endpoint = ParseResponse(request.RawUrl);
                await GetAndSendResponse(endpoint, response);
            }

            _http.Stop();
        }

        private void InitPrefixes()
        {
            _http.Prefixes.Add($"{_uri}/");
            string[] prefixes = Enum.GetNames(typeof(UrlType));
            foreach (string p in prefixes)
            {
                _http.Prefixes.Add($"{_uri}/{p}/");
            }
        }

        private UrlType ParseResponse(string url)
        {
            var relativePath = url.Trim('/');
            Enum.TryParse(relativePath, true, out UrlType endpoint);
            return endpoint;
        }

        private async Task GetAndSendResponse(UrlType endpoint, HttpListenerResponse response)
        {
            switch (endpoint)
            {
                case UrlType.MyName:
                    {
                        await GetMyName(response);
                        break;
                    }
                case UrlType.Information:
                    {
                        await Information(response);
                        break;
                    }
                case UrlType.Success:
                    {
                        await Success(response);
                        break;
                    }
                case UrlType.Redirection:
                    {
                        await Redirection(response);
                        break;
                    }
                case UrlType.ClientError:
                    {
                        await ClientError(response);
                        break;
                    }
                case UrlType.ServerError:
                    {
                        await ServerError(response);
                        break;
                    }
                case UrlType.MyNameByHeader:
                    {
                        await GetMyNameByHeader(response);
                        break;
                    }
                case UrlType.MyNameByCookies:
                    {
                        await GetMyNameByCookies(response);
                        break;
                    }
                default:
                    break;
            }
        }

        private async Task GetMyName(HttpListenerResponse response)
        {
            response.StatusCode = 200;
            await SendResponse(response, _name);
        }

        private async Task Information(HttpListenerResponse response)
        {
            response.StatusCode = 101;
            await SendResponse(response, "The request was received, continuing process");
        }

        private async Task Success(HttpListenerResponse response)
        {
            response.StatusCode = 200;
            await SendResponse(response, "The request was successfully received, understood, and accepted");
        }

        private async Task Redirection(HttpListenerResponse response)
        {
            response.StatusCode = 300;
            await SendResponse(response, "Further action needs to be taken in order to complete the request");
        }

        private async Task ClientError(HttpListenerResponse response)
        {
            response.StatusCode = 404;
            await SendResponse(response, "Not found");
        }

        private async Task ServerError(HttpListenerResponse response)
        {
            response.StatusCode = 500;
            await SendResponse(response, "The server failed to fulfil an apparently valid request");
        }

        private async Task GetMyNameByHeader(HttpListenerResponse response)
        {
            response.StatusCode = 200;
            response.AddHeader("X-MyName", _name);
            await SendResponse(response, "Successfully send name by header");
        }

        private async Task GetMyNameByCookies(HttpListenerResponse response)
        {
            response.StatusCode = 200;
            Cookie cookie = new Cookie("MyName", _name);
            response.AppendCookie(cookie);
            await SendResponse(response, "Successfully send name by cookies");
        }

        private async Task SendResponse(HttpListenerResponse response, string responseString)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(responseString);
            response.ContentLength64 = buffer.Length;
            Stream output = response.OutputStream;
            await output.WriteAsync(buffer, 0, buffer.Length);
            output.Close();
        }
    }
}
