﻿using System.Net;
using System.Threading.Tasks;

namespace Listener
{
    internal class Program
    {
        static void Main(string[] args)
        {
            MainAsync().Wait();
        }
        static async Task MainAsync()
        {
            string baseUri = "http://localhost:8888";
            HttpListener http = new HttpListener();
            Listener listener = new Listener(http, baseUri);
            await listener.StartListening();
        }
    }
}
